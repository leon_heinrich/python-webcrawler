import pandas as pd
import requests
from bs4 import BeautifulSoup
import time

stocks = [
    # Enter stock keywords fitting to url below, example:
    "AAPL",
    "MSFT",
    "AMZN"
]

ev_sales = []
gross_margin = []
rule_of_40 = []
sales_increase = []
debt_div_equity = []
peg_ratio = []
div_indicator = []

start = time.time()

def convertToMillion(string_value):
    return_value = 0
    if string_value[-1] == "B":
        return_value = float(string_value[:-1].replace(",", ".")) * 1000
    elif string_value[-1] == "T":
        return_value = float(string_value[:-1].replace(",", ".")) * 1000 * 1000
    else:
        return_value = float(string_value[:-1].replace(",", "."))

    return return_value

for stock in stocks:
    page = requests.get(f"https://de.finance.yahoo.com/quote/{stock}/key-statistics?p={stock}")
    soup = BeautifulSoup(page.content, 'html.parser')

    chart = soup.find(id="Col1-0-KeyStatistics-Proxy")

    # ALL NUMBERS IN MILLION
    ### EV/Sales
    ev_sales_number = float(chart.find_all("table")[0].find_all("tr")[7].find_all("td")[1].get_text().replace(",", "."))

    ev_sales_result = 0
    if ev_sales_number <= 8:
        ev_sales_result = 3
    elif 8 < ev_sales_number <= 10:
        ev_sales_result = 2
    elif 10 < ev_sales_number <= 12:
        ev_sales_result = 1

    ev_sales.append(ev_sales_result)

    ### Gross Margin
    # Gross Profit
    gross_profit_text = chart.find_all("table")[7].find_all("tr")[3].find_all("td")[1].get_text()
    gross_profit_number = convertToMillion(gross_profit_text)

    # Sales
    sales_text = chart.find_all("table")[7].find_all("tr")[0].find_all("td")[1].get_text()
    sales_number = convertToMillion(sales_text)

    gross_margin_number = gross_profit_number / sales_number
    gross_margin_result = 0
    if gross_margin_number >= 0.75:
        gross_margin_result = 3
    elif 0.75 > gross_margin_number >= 0.65:
        gross_margin_result = 2
    elif 0.65 > gross_margin_number >= 0.5:
        gross_margin_result = 1

    gross_margin.append(gross_margin_result)

    ### Rule of 40
    ebitda_text = chart.find_all("table")[7].find_all("tr")[4].find_all("td")[1].get_text()
    ebitda_number = convertToMillion(ebitda_text)

    sales_incr_text = chart.find_all("table")[7].find_all("tr")[2].find_all("td")[1].get_text()
    sales_incr_number = float(sales_incr_text.replace(",", ".").replace("%", ""))/100

    rule_of_40_number = ebitda_number / sales_number + sales_incr_number
    rule_of_40_result = 0
    if rule_of_40_number >= 0.60:
        rule_of_40_result = 3
    elif 0.6 > rule_of_40_number >= 0.5:
        rule_of_40_result = 2
    elif 0.5 > rule_of_40_number >= 0.4:
        rule_of_40_result = 1
    
    rule_of_40.append(rule_of_40_result)

    ### Sales Increase
    sales_incr_result = 0
    if sales_incr_number >= 0.40:
        sales_incr_result = 3
    elif 0.4 > sales_incr_number >= 0.3:
        sales_incr_result = 2
    elif 0.3 > sales_incr_number >= 0.2:
        sales_incr_result = 1

    sales_increase.append(sales_incr_result)

    ### Debt/Equity-Ratio
    debt_equity_number = float(chart.find_all("table")[8].find_all("tr")[3].find_all("td")[1].get_text().replace(",", "."))/100
    debt_equity_result = 0
    if debt_equity_number <= 0.5:
        debt_equity_result = 3
    elif 0.5 < debt_equity_number <= 1:
        debt_equity_result = 2
    elif 1 < debt_equity_number <= 1.5:
        debt_equity_result = 1

    debt_div_equity.append(debt_equity_result)

    ### PEG-Ratio
    peg_ratio_number = float(chart.find_all("table")[0].find_all("tr")[4].find_all("td")[1].get_text().replace(",", "."))
    peg_ratio_result = 0
    if 0 < peg_ratio_number < 1:
        peg_ratio_result = 1

    peg_ratio.append(peg_ratio_result)

    ### No Dividend
    div_value = chart.find_all("table")[3].find_all("tr")[1].find_all("td")[1].get_text()
    div_result = ""
    if div_value == "N/A":
        div_result = "No"
    else: 
        div_result = div_value
    
    div_indicator.append(div_result)

stock_data = pd.DataFrame(
    {
        'Aktie': stocks,
        'EV/Sales': ev_sales,
        'Gross-Margin': gross_margin,
        'RuleOf40': rule_of_40,
        'Sales_Incr': sales_increase,
        'Debt_Equity_Ratio': debt_div_equity,
        'PEG-Ratio': peg_ratio,
        'Div?': div_indicator
    }
)

print(stock_data)

stock_data.to_csv('stocks_high_growth_premium.csv')

end = time.time()
print((end - start)/60)